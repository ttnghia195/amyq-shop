<?php

/**
 * Đặt các đoạn code cần tùy biến của bạn vào bên dưới
 */

/**
 * Tùy biến các mục ngoài trang chủ
 * @hook after_setup_theme
 * 
 * @locate /inc/storefront-template-functions.php
 * @function storefront_product_categories_args
 * @function storefront_featured_products_args
 * @function storefront_popular_products_args
 * @function storefront_on_sale_products_args
 * @function storefront_best_selling_products_args
 */
 
function tp_homepage_blocks() {
 /*
 * Sử dụng: remove_action( 'homepage', 'tên_hàm_cần_xóa', số thứ tự mặc định );
 */
 remove_action( 'homepage', 'storefront_product_categories', 20 );
 remove_action( 'homepage', 'storefront_featured_products', 40 );
 remove_action( 'homepage', 'storefront_popular_products', 50 );
 remove_action( 'homepage', 'storefront_best_selling_products', 70 );
}
add_action( 'after_setup_theme', 'tp_homepage_blocks', 10 );

function tp_homepage_blocks_custom() {
 
 
 /* Shop by Category */
 add_filter( 'storefront_product_categories_args', function($args) {
 $args = array(
 'columns' => 4,
 'limit' => 8,
 'title' => __( 'Danh mục sản phẩm', 'thachpham' )
 );
 
 return $args; 
 } );
 
 /* New In */
 add_filter( 'storefront_recent_products_args', function($args) {
 $args = array(
 'columns' => 4,
 'limit' => 8,
 'title' => __( 'Sản phẩm mới', 'thachpham' )
 );
 return $args;
 } );
  
 /*On Sale*/
 add_filter( 'storefront_best_selling_products_args', function($args) {
 $args = array(
 'columns' => 4,
 'limit' => 8,
 'title' => __( 'Khuyến mãi', 'thachpham' )
 );
 return $args;
 } ); 
 
 
 /* And so on.... */
 
}
add_action( 'after_setup_theme', 'tp_homepage_blocks_custom' );